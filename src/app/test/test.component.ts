import {AfterViewInit, Component, ContentChild, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IonContent, IonRefresher} from "@ionic/angular";
import {interval} from "rxjs";
import {finalize, take} from "rxjs/operators";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements AfterViewInit {

  @ContentChild(IonRefresher, {static: true}) ionRefresher: IonRefresher;
  @ContentChild('refresher', {static: false, read: ElementRef}) refresher: ElementRef;
  @ViewChild(IonContent, {static: false}) ionContent: IonContent;

  constructor(
  ) {
  }

  async ngAfterViewInit() {
    const scrollElement = await this.ionContent.getScrollElement();
    const refresherElement = this.refresher.nativeElement;
    setTimeout(() => {
      this.triggerMouseEvent(scrollElement, 'mousedown');
      const steps = 10;
      interval(50)
        .pipe(take(steps))
        .pipe(finalize(() => this.triggerMouseEvent(refresherElement.ownerDocument.body, 'mouseup')))
        .subscribe(i => {
          const y = (i + 1) * 20 + 200;
          this.mouseMoveY(refresherElement.ownerDocument.body, y);
        });
    }, 0);
  }

  mouseMoveY(element: HTMLElement, y: number) {
    const event = new MouseEvent("mousemove", {
      bubbles: true,
      cancelable: true,
      clientY: y,
      pageY: y,
      screenY: y,
      y: y,
    } as any);
    element.dispatchEvent(event);
  }

  triggerMouseEvent(node: HTMLElement, eventType: string) {
    const mouseEvent = document.createEvent('MouseEvents');
    mouseEvent.initEvent(eventType, true, true);
    node.dispatchEvent(mouseEvent);
  }

}
